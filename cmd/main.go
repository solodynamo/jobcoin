package main

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	mixer "gitlab.com/solodynamo/jobcoin/app"
	"gitlab.com/solodynamo/jobcoin/config"
	"gitlab.com/solodynamo/jobcoin/internal/algorithm"
	"gitlab.com/solodynamo/jobcoin/internal/infrastructure/logger"
	"gitlab.com/solodynamo/jobcoin/internal/polling"
	"gitlab.com/solodynamo/jobcoin/internal/presentation/http/crud"
	"gitlab.com/solodynamo/jobcoin/pkg/providers/gemini/jobcoin"
)

func main() {
	var (
		quit   = make(chan struct{})
		client = &jobcoin.JobcoinClient{
			Client: http.DefaultClient,
		}
		// we can switch distribution algorithm in a flexible way
		algo = algorithm.RandomDistribution{}
		// we can switch polling logic in a flexible way
		pollingStrategy polling.PollingStrategy = &polling.ByTransaction{}
	)

	jobcoinBackend := mixer.NewJobCoinBackendApp(client, &algo, pollingStrategy)
	pollingInterval := time.NewTicker(time.Second * time.Duration(config.Get().PollingIntervalSeconds))
	go jobcoinBackend.Poll(pollingInterval, quit)

	r := mux.NewRouter()
	r.HandleFunc("/mix", crud.InitiateMixingHandler()).Methods("POST")
	logger.Println(fmt.Sprintf("Running the app on port: %v", config.Get().Port))
	log.Fatal(http.ListenAndServe(config.Get().Port, r))
}
