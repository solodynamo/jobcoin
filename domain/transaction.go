package domain

import (
	"fmt"
	"time"

	"gitlab.com/solodynamo/jobcoin/internal/infrastructure/logger"
)

var (
	layout = "2006-01-02T15:04:05.000Z"
)

// JobcoinTx represents the transaction that are returned from api
type JobcoinTx struct {
	Timestamp   string `json:"timestamp"`
	FromAddress string `json:"fromAddress"`
	ToAddress   string `json:"toAddress"`
	Amount      string `json:"amount"`
}

func (tx *JobcoinTx) GetTimestamp() (time.Time, error) {
	// UTC
	t, err := time.Parse(layout, tx.Timestamp)
	if err != nil {
		logger.Println(fmt.Sprintf("unable to parse time: %v", err))
		return time.Time{}, err
	}
	return t, nil
}
