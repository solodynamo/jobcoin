package domain

import (
	"testing"
	"time"

	"github.com/stretchr/testify/suite"
	"gitlab.com/solodynamo/jobcoin/internal/repository"
)

type DomainTestSuite struct {
	suite.Suite
}

func (s *DomainTestSuite) TestGetTimestamp() {
	s.Run("valid case", func() {
		m := JobcoinTx{
			Timestamp:   "2021-10-28T02:43:48.881Z",
			FromAddress: repository.GetRandomHash(),
			ToAddress:   repository.GetRandomHash(),
			Amount:      "1",
		}
		tm, err := m.GetTimestamp()
		s.Require().NoError(err)

		dummyTime, err := time.Parse(layout, "2021-09-28T02:43:48.881Z")
		s.Require().NoError(err)

		s.Require().True(tm.After(dummyTime))
	})
	s.Run("invalid case", func() {
		m := JobcoinTx{
			Timestamp:   "27 july 2021",
			FromAddress: repository.GetRandomHash(),
			ToAddress:   repository.GetRandomHash(),
			Amount:      "1",
		}
		_, err := m.GetTimestamp()
		s.Require().Error(err)
	})
}

func TestDomainTestSuite(t *testing.T) {
	suite.Run(t, new(DomainTestSuite))
}
