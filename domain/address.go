package domain

// JobcoinAddress represents info for an address
type JobcoinAddress struct {
	Balance      string      `json:"balance"`
	Transactions []JobcoinTx `json:"transactions"`
}
