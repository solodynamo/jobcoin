package domain

// InitiateMixingRequest represents request payload for mixing
type InitiateMixingRequest struct {
	DestinationAddresses []string `json:"destination_addresses"`
}

// InitiateMixingRequest represents response payload after initiating
type InitiateMixingResponse struct {
	DepositAddress string `json:"deposit_address"`
}
