package mixer

import (
	"time"

	"gitlab.com/solodynamo/jobcoin/internal/algorithm"
	"gitlab.com/solodynamo/jobcoin/internal/polling"
	"gitlab.com/solodynamo/jobcoin/pkg/providers/gemini/jobcoin"
)

// JobCoinBackend is entry point to init our app with deps required
type JobCoinBackend struct {
	Poller polling.PollingStrategy
}

// NewJobCoinBackendApp returns new jobcoin mixing app
func NewJobCoinBackendApp(
	client jobcoin.APIClient,
	algo algorithm.DistributionAlgorithm,
	pollingStrategy polling.PollingStrategy,
) *JobCoinBackend {
	return &JobCoinBackend{
		Poller: pollingStrategy.New(client, algo),
	}
}

// PollForNewDeposits is an endlessly looping function handling the input of new users.
func (b *JobCoinBackend) Poll(ticker *time.Ticker, shutdown <-chan struct{}) {
	b.Poller.Poll(ticker, shutdown)
}
