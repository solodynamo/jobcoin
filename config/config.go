package config

type ServerConfig struct {
	GeminiBaseURL          string
	Port                   string
	PollingIntervalSeconds int
}

// GetConfig gets the server config.
// normally this will stored in some secret vault and parsed through env in our config
func Get() ServerConfig {
	return ServerConfig{
		GeminiBaseURL:          "http://jobcoin.gemini.com/violate-march/api",
		Port:                   ":8085",
		PollingIntervalSeconds: 6,
	}
}
