# Go parameters
GOCMD=go
GOBUILD=$(GOCMD) build
GOCLEAN=$(GOCMD) clean
GOTEST=$(GOCMD) test
GOGET=$(GOCMD) get
APP_NAME=jobcoin-mixer

.PHONY: all
all: clean deps build

.PHONY: run
run: 
		./$(APP_NAME)

.PHONY: build
build: deps
		$(GOBUILD) -o $(APP_NAME) -v cmd/main.go

.PHONY: clean
clean:
		$(GOCLEAN)
		rm -f $(APP_NAME)

.PHONY: deps
deps:
		$(GOCMD) mod download

.PHONY: setup-mock-lib
setup-mock-lib:
		brew install mockery

.PHONY: clean-test
clean-test:
	rm -rf c.out && $(GOCMD) clean -testcache

.PHONY: test
test: clean-test deps
	$(GOCMD) test -count=1 -race -coverprofile=c.out ./...

.PHONY: test-coverage
test-coverage: test
	$(GOCMD) tool cover -html=c.out

.PHONY: generate-docs
generate-docs:
	$(GOCMD) generate ./...