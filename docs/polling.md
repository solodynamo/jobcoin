# Polling


Some scenarios I can think of after going over provided API docs:

1. User wants to send some coins at address addr1, addr2 calls /mix
    - Now they will get "some-random-hash-address" to which they can send coins later.
Conclusion: My mixer backend should be able to poll such addresss for "balance" and send over the money to those "addr"s once the funds are available.
Conclusion: I can poll by balances at some interval gap.


2. I have transactions API, call it to fetch transactions, find transactions we care of(don't process older onces using timestamp attribute in response) and if that particular transaction's toAddress has balance and also has an entry as key in our local DB, which means a mixing request was requested for this address, so now we have funds, go forward and send over the money to those "addr"s.


## Thinking of tradeoffs:

Polling by Address:
Pros:
- In production, there will be ton of transactions and this data will grow at much faster pace. Also, Transactions API doesn't accept any date range so we cannot query to get only latest ones... which means we will be fetching ton of transactions we actually don't need to process(as they are older). 
    - This will consume lot of network bandwidth as there will be monotonically increasing transaction objects most of which we don't care about but still will be paying for infra network costs.
    - It might get slow gradually to parse so much of data so waste of CPU and memory.

Cons:
- As the deposit addresses/users grows, this approach won't scale and will have problems related to delay in processing mixing requests for those addresses.
- If we don't remove the mapping once used to transfer funds, same mapping can be used in future to transfer funds in a transitive manner.
ex: addr1 -> addr2, addr3 ...... addr 4 -> addr1 ... now as old mapping is present .. the funds will land up at addr2 and addr3

Polling by Transactions:
Pros:
- Instead of bruteforcing all the accounts entries in our DB to check balances, just get the recent transactions and proceed as per that.

Refer [Optimizing Transactions](#optimizing-transactions)

Cons:
- Transactions API doesn't accept any date range so we cannot query to get only latest ones... which means we will be fetching ton of transactions we actually don't need to process(as they are older)


General problems with polling HTTP connections
- If we are using TCP to fetch all transactions data, then there can be packet loss too(as there will be lot of data) which will lead to TCP packet retransmission which will add to overall response delay.

- If our backend doesn't do TCP connection pooling, there can be TCP slow-start overhead which will be further amplied in a slow network


## Optimizing Transactions
- *Transactions will be more efficient to use at production scale*, we can get over its con of "all" transaction at once by consuming those from a message queue, this way we will have close to FIFO order and at same time operate on newer transactions only.

- It will also put less pressure on DB, as in above transactions API we are basically scanning the transaction table which will put lot of load on the DB.