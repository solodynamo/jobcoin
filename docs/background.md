What is coin mixing?
1. One of the techniques used to enhance bitcoin transaction privacy.
2. Good coin mixing algo can prevent most transaction analysis attacks.
3. Most coin mixing algos do not change the bitcoin protocol so these are no different than regular transactions.


Some public mixing service: ChipMixer, MixTum, Bitcoin Mixer, CryptoMixer, and Sudoku Wallet

So on what input domain my algo will do the mixing/obfuscation?
1. User Input address(not always present tho)
2. Mixer input address(To bypass above limitation, we can generate random input address for each user of the mixer)
3. Amount being transferred - Mixing fees(optional) and distribution over multiple output addresses
4. Time and date of input and output transactions - Mixing delays

Goals
- Try to obfuscate the transactions as much as possible as all of those are visible on public blockchain(not in jobcoin's case), no one should be able to guess. Random distribution helps.