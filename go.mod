module gitlab.com/solodynamo/jobcoin

go 1.16

require (
	github.com/ProtonMail/go-crypto v0.0.0-20210920160938-87db9fbc61c7 // indirect
	github.com/fatih/color v1.13.0 // indirect
	github.com/go-git/go-git/v5 v5.4.2 // indirect
	github.com/gorilla/mux v1.8.0
	github.com/mattn/go-colorable v0.1.11 // indirect
	github.com/mattn/go-runewidth v0.0.13 // indirect
	github.com/princjef/gomarkdoc v0.2.1
	github.com/princjef/mageutil v1.0.0 // indirect
	github.com/satori/go.uuid v1.2.0
	github.com/spf13/cobra v1.2.1 // indirect
	github.com/spf13/viper v1.9.0 // indirect
	github.com/stretchr/objx v0.3.0 // indirect
	github.com/stretchr/testify v1.7.0
	github.com/xanzy/ssh-agent v0.3.1 // indirect
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519 // indirect
	golang.org/x/net v0.0.0-20210929193557-e81a3d93ecf6 // indirect
	golang.org/x/sys v0.0.0-20211002104244-808efd93c36d // indirect
	golang.org/x/term v0.0.0-20210927222741-03fcf44c2211 // indirect
	golang.org/x/text v0.3.7 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
	mvdan.cc/xurls/v2 v2.3.0 // indirect
)
