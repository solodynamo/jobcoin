package polling

import (
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	"gitlab.com/solodynamo/jobcoin/domain"
	"gitlab.com/solodynamo/jobcoin/internal/algorithm"
	"gitlab.com/solodynamo/jobcoin/internal/repository"
	"gitlab.com/solodynamo/jobcoin/pkg/providers/gemini/jobcoin/mocks"
)

type PollByBalancesTestSuite struct {
	suite.Suite
}

func (q *PollByBalancesTestSuite) AfterTest(_, _ string) {

}

func (s *PollByBalancesTestSuite) TestNew() {
	m := ByBalances{}
	client := &mocks.APIClient{}
	algo := algorithm.RandomDistribution{}
	s.Require().NotEmpty(m.New(client, &algo))
}

func (s *PollByBalancesTestSuite) TestPoll() {
	pollingInterval := time.NewTicker(time.Second * 1)
	client := &mocks.APIClient{}
	originalDepositAddr := repository.GetRandomHash()
	destinationAddrs := []string{repository.GetRandomHash(), repository.GetRandomHash()}
	prepareMockResponses := func() {
		client.On("GetAddressInfo", originalDepositAddr).Return(domain.JobcoinAddress{
			Balance: "100",
			Transactions: []domain.JobcoinTx{
				{
					// for polling by balances, we don't really care of below transactions data.
					Timestamp:   time.Now().String(),
					FromAddress: repository.GetRandomHash(),
					ToAddress:   repository.GetRandomHash(),
					Amount:      "10",
				},
			},
		}, nil).Once()

		client.On("SendJobcoin", mock.MatchedBy(func(depositAddr string) bool {
			s.Equal(originalDepositAddr, depositAddr)
			return true
		}), mock.MatchedBy(func(destinationAddr string) bool {
			s.Contains(destinationAddrs, destinationAddr)
			return true
		}), mock.MatchedBy(func(amt string) bool {
			s.NotEqual(amt, "0.0")
			return true
		})).Return(nil).Times(len(destinationAddrs))
		// should be called exactly no of destination addresses times.
	}
	populateDB := func() {
		repository.GetDB().Store(
			originalDepositAddr,
			destinationAddrs,
		)
	}
	clearDB := func() {
		repository.GetDB().Delete(originalDepositAddr)
	}
	prepareMockResponses()
	populateDB()

	algo := algorithm.RandomDistribution{}
	m := ByBalances{
		Client:    client,
		Algorithm: &algo,
	}

	// we can have text fixtures,
	// but as its just simple map so doing below way.
	quit := make(chan struct{})
	go m.Poll(pollingInterval, quit)
	// quit goroutine
	time.Sleep(1 * time.Second)
	clearDB()
	quit <- struct{}{}
}

func (s *PollByBalancesTestSuite) TestPoll_AddressAPI() {
	s.Run("address API returns error", func() {
		pollingInterval := time.NewTicker(time.Second * 1)
		client := &mocks.APIClient{}
		originalDepositAddr := repository.GetRandomHash()
		destinationAddrs := []string{repository.GetRandomHash(), repository.GetRandomHash()}
		prepareMockResponses := func() {
			// mocking addressAPI to return error
			client.On("GetAddressInfo", originalDepositAddr).Return(domain.JobcoinAddress{}, fmt.Errorf("unable to process request")).Once()

			client.On("SendJobcoin", mock.MatchedBy(func(depositAddr string) bool {
				s.Equal(originalDepositAddr, depositAddr)
				return true
			}), mock.MatchedBy(func(destinationAddr string) bool {
				s.Contains(destinationAddrs, destinationAddr)
				return true
			}), mock.MatchedBy(func(amt string) bool {
				s.NotEqual(amt, "0.0")
				return true
			})).Return(nil).Times(0)
			// Send jobcoin should not be called at all, as address api failed
		}
		populateDB := func() {
			repository.GetDB().Store(
				originalDepositAddr,
				destinationAddrs,
			)
		}
		clearDB := func() {
			repository.GetDB().Delete(originalDepositAddr)
		}
		prepareMockResponses()
		populateDB()

		algo := algorithm.RandomDistribution{}
		m := ByBalances{
			Client:    client,
			Algorithm: &algo,
		}

		// we can have text fixtures,
		// but as its just simple map so doing below way.
		quit := make(chan struct{})
		go m.Poll(pollingInterval, quit)
		// quit goroutine
		time.Sleep(1 * time.Second)
		clearDB()
		quit <- struct{}{}
	})
}

func (s *PollByBalancesTestSuite) TestPoll_TransactionAPI() {
	s.Run("transaction API returns error", func() {
		pollingInterval := time.NewTicker(time.Second * 1)
		client := &mocks.APIClient{}
		originalDepositAddr := repository.GetRandomHash()
		destinationAddrs := []string{repository.GetRandomHash(), repository.GetRandomHash()}
		prepareMockResponses := func() {
			// mocking addressAPI to return error
			client.On("GetAddressInfo", originalDepositAddr).Return(domain.JobcoinAddress{
				Balance: "100",
				Transactions: []domain.JobcoinTx{
					{
						// for polling by balances, we don't really care of below transactions data.
						Timestamp:   time.Now().String(),
						FromAddress: repository.GetRandomHash(),
						ToAddress:   repository.GetRandomHash(),
						Amount:      "10",
					},
				},
			}, nil).Once()

			client.On("SendJobcoin", mock.MatchedBy(func(depositAddr string) bool {
				s.Equal(originalDepositAddr, depositAddr)
				return true
			}), mock.MatchedBy(func(destinationAddr string) bool {
				s.Contains(destinationAddrs, destinationAddr)
				return true
			}), mock.MatchedBy(func(amt string) bool {
				s.NotEqual(amt, "0.0")
				return true
			})).Return(fmt.Errorf("unable to process request")).Times(len(destinationAddrs))
			// Send jobcoin returns erros
		}
		populateDB := func() {
			repository.GetDB().Store(
				originalDepositAddr,
				destinationAddrs,
			)
		}
		clearDB := func() {
			repository.GetDB().Delete(originalDepositAddr)
		}
		prepareMockResponses()
		populateDB()

		algo := algorithm.RandomDistribution{}
		m := ByBalances{
			Client:    client,
			Algorithm: &algo,
		}

		// we can have text fixtures,
		// but as its just simple map so doing below way.
		quit := make(chan struct{})
		go m.Poll(pollingInterval, quit)
		// quit goroutine
		time.Sleep(1 * time.Second)
		clearDB()
		quit <- struct{}{}
	})
}
func TestPollByBalancesTestSuite(t *testing.T) {
	suite.Run(t, new(PollByBalancesTestSuite))
}
