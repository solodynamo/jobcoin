<!-- Code generated by gomarkdoc. DO NOT EDIT -->

# polling

```go
import "gitlab.com/solodynamo/jobcoin/internal/polling"
```

## Index

- [type ByBalances](<#type-bybalances>)
  - [func (m *ByBalances) DispatchAmounts(addresses []string, amounts []float64, totalAmount string, depositAddress string) error](<#func-bybalances-dispatchamounts>)
  - [func (m *ByBalances) New(
    client jobcoin.APIClient,
    algo algorithm.DistributionAlgorithm) PollingStrategy](<#func-bybalances-new>)
  - [func (m *ByBalances) Poll(ticker *time.Ticker, shutdown <-chan struct{})](<#func-bybalances-poll>)
- [type ByTransaction](<#type-bytransaction>)
  - [func (m *ByTransaction) DispatchAmounts(addresses []string, amounts []float64, totalAmount string, despositAccount string) error](<#func-bytransaction-dispatchamounts>)
  - [func (m *ByTransaction) New(
    client jobcoin.APIClient,
    algo algorithm.DistributionAlgorithm) PollingStrategy](<#func-bytransaction-new>)
  - [func (m *ByTransaction) Poll(ticker *time.Ticker, shutdown <-chan struct{})](<#func-bytransaction-poll>)
- [type PollingStrategy](<#type-pollingstrategy>)


## type ByBalances

ByBalances implementes PollingStrategy to poll using balances

```go
type ByBalances struct {
    Client    jobcoin.APIClient
    Algorithm algorithm.DistributionAlgorithm
}
```

### func \(\*ByBalances\) DispatchAmounts

```go
func (m *ByBalances) DispatchAmounts(addresses []string, amounts []float64, totalAmount string, depositAddress string) error
```

DispatchAmounts sends jobcoins to passed addresses as per their respective amounts decided as per DistributionAlgorithm

### func \(\*ByBalances\) New

```go
func (m *ByBalances) New(
    client jobcoin.APIClient,
    algo algorithm.DistributionAlgorithm) PollingStrategy
```

New return an instance of ByBalances polling strategy

### func \(\*ByBalances\) Poll

```go
func (m *ByBalances) Poll(ticker *time.Ticker, shutdown <-chan struct{})
```

Poll polls using deposit addresses in our db and check for balances in a regular time interval

## type ByTransaction

ByBalances implementes PollingStrategy to poll using transactions

```go
type ByTransaction struct {
    Client    jobcoin.APIClient
    Algorithm algorithm.DistributionAlgorithm
}
```

### func \(\*ByTransaction\) DispatchAmounts

```go
func (m *ByTransaction) DispatchAmounts(addresses []string, amounts []float64, totalAmount string, despositAccount string) error
```

DispatchAmounts sends jobcoins to passed addresses as per their respective amounts decided as per DistributionAlgorithm

### func \(\*ByTransaction\) New

```go
func (m *ByTransaction) New(
    client jobcoin.APIClient,
    algo algorithm.DistributionAlgorithm) PollingStrategy
```

New return an instance of ByTransaction polling strategy

### func \(\*ByTransaction\) Poll

```go
func (m *ByTransaction) Poll(ticker *time.Ticker, shutdown <-chan struct{})
```

Poll polls by calling Transactions API and only checking transactions after \`lastTimestamp\` and then checking if a mixing request exists in our DB and then proceeds

## type PollingStrategy

PollingStrategy represents interface for polling strategy for mixing requests

```go
type PollingStrategy interface {
    New(jobcoin.APIClient, algorithm.DistributionAlgorithm) PollingStrategy
    Poll(t *time.Ticker, shutdown <-chan struct{})
    DispatchAmounts(addresses []string, amounts []float64, totalAmount string, despositAccount string) error
}
```



Generated by [gomarkdoc](<https://github.com/princjef/gomarkdoc>)
