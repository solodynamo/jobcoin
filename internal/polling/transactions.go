package polling

import (
	"fmt"
	"time"

	"gitlab.com/solodynamo/jobcoin/internal/algorithm"
	"gitlab.com/solodynamo/jobcoin/internal/infrastructure/logger"
	"gitlab.com/solodynamo/jobcoin/internal/repository"
	"gitlab.com/solodynamo/jobcoin/pkg/providers/gemini/jobcoin"
	"gitlab.com/solodynamo/jobcoin/pkg/utils"
)

var (
	// this is to process transaction in a time series manner
	lastTimestamp = time.Time{}
)

// ByBalances implementes PollingStrategy to poll using transactions
type ByTransaction struct {
	Client    jobcoin.APIClient
	Algorithm algorithm.DistributionAlgorithm
}

// New return an instance of ByTransaction polling strategy
func (m *ByTransaction) New(
	client jobcoin.APIClient,
	algo algorithm.DistributionAlgorithm) PollingStrategy {
	return &ByTransaction{
		Client:    client,
		Algorithm: algo,
	}
}

// Poll polls by calling Transactions API and only checking
// transactions after `lastTimestamp` and then checking if
// a mixing request exists in our DB and then proceeds
func (m *ByTransaction) Poll(ticker *time.Ticker, shutdown <-chan struct{}) {
	for {
		select {
		case <-shutdown:
			return
		case <-ticker.C:
			transactions, err := m.Client.GetTransactions()
			if err != nil {
				// might be due to some intermittent n/w issue, continue;
				// in a production setting, to avoid cascading failures
				// we can employ circuit breaking with exponential backoff retries.
				continue
			}
			for _, t := range transactions {
				ts, err := t.GetTimestamp()
				if err != nil {
					logger.Println(fmt.Sprintf("ignoring transaction due to time format parse error %v", err))
					continue
				}
				addresses, isPresent := repository.Get(t.ToAddress)
				if ts.After(lastTimestamp) && isPresent {
					lastTimestamp = ts

					// we can add step here to deduct a fee here before distributing the amount

					amounts, _ := m.Algorithm.GetBalances(addresses, t.Amount)
					var errs []error
					derr := m.DispatchAmounts(
						addresses,
						amounts,
						t.Amount,
						t.ToAddress,
					)
					if derr != nil {
						errs = append(errs, derr)
					}

					if len(errs) != 0 {
						logger.Println("dispatching to some destination addresses failed deposit address" + t.ToAddress)
					}
				}
			}
		}
	}
}

// DispatchAmounts sends jobcoins to passed addresses as per their respective amounts decided as per DistributionAlgorithm
func (m *ByTransaction) DispatchAmounts(addresses []string, amounts []float64, totalAmount string, despositAccount string) error {
	floatTotalAmt, err := utils.GetFloat64(totalAmount)
	if err != nil {
		logger.Println("unable to convert total amount to float64")
		return err
	}
	if len(addresses) > 0 && floatTotalAmt > float64(0.0) {
		for idx, add := range addresses {
			m.Client.SendJobcoin(despositAccount, add, fmt.Sprintf("%f", amounts[idx]))
		}
	}
	return nil
}
