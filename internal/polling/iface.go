package polling

import (
	"time"

	"gitlab.com/solodynamo/jobcoin/internal/algorithm"
	"gitlab.com/solodynamo/jobcoin/pkg/providers/gemini/jobcoin"
)

// PollingStrategy represents interface for polling strategy for mixing requests
type PollingStrategy interface {
	New(jobcoin.APIClient, algorithm.DistributionAlgorithm) PollingStrategy
	Poll(t *time.Ticker, shutdown <-chan struct{})
	DispatchAmounts(addresses []string, amounts []float64, totalAmount string, despositAccount string) error
}
