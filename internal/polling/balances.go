package polling

import (
	"fmt"
	"time"

	"gitlab.com/solodynamo/jobcoin/internal/algorithm"
	"gitlab.com/solodynamo/jobcoin/internal/infrastructure/logger"
	"gitlab.com/solodynamo/jobcoin/internal/repository"
	"gitlab.com/solodynamo/jobcoin/pkg/providers/gemini/jobcoin"
	"gitlab.com/solodynamo/jobcoin/pkg/utils"
)

// ByBalances implementes PollingStrategy to poll using balances
type ByBalances struct {
	Client    jobcoin.APIClient
	Algorithm algorithm.DistributionAlgorithm
}

// New return an instance of ByBalances polling strategy
func (m *ByBalances) New(
	client jobcoin.APIClient,
	algo algorithm.DistributionAlgorithm) PollingStrategy {
	return &ByBalances{
		Client:    client,
		Algorithm: algo,
	}
}

// Poll polls using deposit addresses in our db and check for balances in a regular time interval
func (m *ByBalances) Poll(ticker *time.Ticker, shutdown <-chan struct{}) {
	for {
		select {
		case <-shutdown:
			return
		case <-ticker.C:
			repository.GetDB().Range(func(key, value interface{}) bool {
				sourceAddress := key.(string)
				addresses := value.([]string)
				balance, err := m.Client.GetAddressInfo(sourceAddress)
				if err != nil {
					logger.Println("unable to get address info")
					return false
				}

				// we can add step here to deduct a fee here before distributing the amount

				amounts, _ := m.Algorithm.GetBalances(addresses, balance.Balance)
				var errs []error
				derr := m.DispatchAmounts(addresses, amounts, balance.Balance, sourceAddress)
				if err != nil {
					errs = append(errs, derr)
				}
				// we can have retries on this in a production setting
				if len(errs) != 0 {
					logger.Println("dispatching to some destination addresses failed deposit address" + sourceAddress)
				}
				// returning true to keep iterating forward
				return true
			})
		}
	}
}

// DispatchAmounts sends jobcoins to passed addresses as per their respective amounts decided as per DistributionAlgorithm
func (m *ByBalances) DispatchAmounts(addresses []string, amounts []float64, totalAmount string, depositAddress string) error {
	floatTotalAmt, err := utils.GetFloat64(totalAmount)
	if err != nil {
		logger.Println("unable to convert total amount to float64")
		return err
	}
	if len(addresses) > 0 && floatTotalAmt > float64(0.0) {
		for idx, destinationAddress := range addresses {
			m.Client.SendJobcoin(depositAddress, destinationAddress, fmt.Sprintf("%f", amounts[idx]))
		}
	}
	return nil
}
