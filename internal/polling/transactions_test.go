package polling

import (
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	"gitlab.com/solodynamo/jobcoin/domain"
	"gitlab.com/solodynamo/jobcoin/internal/algorithm"
	"gitlab.com/solodynamo/jobcoin/internal/repository"
	"gitlab.com/solodynamo/jobcoin/pkg/providers/gemini/jobcoin/mocks"
)

type PollByTransactionsTestSuite struct {
	suite.Suite
}

func (s *PollByTransactionsTestSuite) TestPoll() {
	pollingInterval := time.NewTicker(time.Second * 1)
	client := &mocks.APIClient{}
	originalDepositAddr := repository.GetRandomHash()
	destinationAddrs := []string{repository.GetRandomHash(), repository.GetRandomHash()}
	prepareMockResponses := func() {
		client.On("GetTransactions").Return([]domain.JobcoinTx{
			{
				Timestamp:   "2021-10-28T02:43:48.881Z",
				FromAddress: repository.GetRandomHash(),
				ToAddress:   repository.GetRandomHash(),
				Amount:      "10",
			},
			{
				Timestamp:   "2021-09-28T02:43:48.881Z",
				FromAddress: repository.GetRandomHash(),
				ToAddress:   repository.GetRandomHash(),
				Amount:      "10",
			},
			{
				Timestamp:   "2021-11-28T02:43:48.881Z",
				FromAddress: repository.GetRandomHash(),
				ToAddress:   originalDepositAddr, // one of tx is for our deposit addr in DB!
				Amount:      "10",
			},
		}, nil).Once()

		client.On("SendJobcoin", mock.MatchedBy(func(depositAddr string) bool {
			s.Equal(originalDepositAddr, depositAddr)
			return true
		}), mock.MatchedBy(func(destinationAddr string) bool {
			s.Contains(destinationAddrs, destinationAddr)
			return true
		}), mock.MatchedBy(func(amt string) bool {
			// this can be 0 so below check isn't that strict
			s.NotEqual(amt, "0.0")
			return true
		})).Return(nil).Times(len(destinationAddrs))
		// should be called exactly no of destination addresses times.
	}
	populateDB := func() {
		repository.GetDB().Store(
			originalDepositAddr,
			destinationAddrs,
		)
	}
	prepareMockResponses()
	populateDB()

	algo := algorithm.RandomDistribution{}
	m := ByTransaction{
		Client:    client,
		Algorithm: &algo,
	}

	// we can have text fixtures,
	// but as its just simple map so doing below way.
	quit := make(chan struct{})
	go m.Poll(pollingInterval, quit)
	// quit goroutine
	time.Sleep(1 * time.Second)
	quit <- struct{}{}
}

func (s *PollByTransactionsTestSuite) TestPoll_AddressAPI() {
	s.Run("transactions GET API returns error", func() {
		pollingInterval := time.NewTicker(time.Second * 1)
		client := &mocks.APIClient{}
		originalDepositAddr := repository.GetRandomHash()
		destinationAddrs := []string{repository.GetRandomHash(), repository.GetRandomHash()}
		prepareMockResponses := func() {
			client.On("GetTransactions").Return([]domain.JobcoinTx{}, fmt.Errorf("unable to process request")).Once()

			client.On("SendJobcoin", mock.MatchedBy(func(depositAddr string) bool {
				s.Equal(originalDepositAddr, depositAddr)
				return true
			}), mock.MatchedBy(func(destinationAddr string) bool {
				s.Contains(destinationAddrs, destinationAddr)
				return true
			}), mock.MatchedBy(func(amt string) bool {
				// this can be 0 so below check isn't that strict
				s.NotEqual(amt, "0.0")
				return true
			})).Return(nil).Times(0)
			// sendjobcoin should not be called at all.
		}
		populateDB := func() {
			repository.GetDB().Store(
				originalDepositAddr,
				destinationAddrs,
			)
		}
		prepareMockResponses()
		populateDB()

		algo := algorithm.RandomDistribution{}
		m := ByTransaction{
			Client:    client,
			Algorithm: &algo,
		}

		// we can have text fixtures,
		// but as its just simple map so doing below way.
		quit := make(chan struct{})
		go m.Poll(pollingInterval, quit)
		// quit goroutine
		time.Sleep(1 * time.Second)
		quit <- struct{}{}
	})
}

func (s *PollByTransactionsTestSuite) TestPoll_TransactionAPI() {
	s.Run("transaction POST API returns error", func() {
		pollingInterval := time.NewTicker(time.Second * 1)
		client := &mocks.APIClient{}
		originalDepositAddr := repository.GetRandomHash()
		destinationAddrs := []string{repository.GetRandomHash(), repository.GetRandomHash()}
		prepareMockResponses := func() {
			client.On("GetTransactions").Return([]domain.JobcoinTx{
				{
					Timestamp:   "2021-10-28T02:43:48.881Z",
					FromAddress: repository.GetRandomHash(),
					ToAddress:   repository.GetRandomHash(),
					Amount:      "10",
				},
				{
					Timestamp:   "2021-09-28T02:43:48.881Z",
					FromAddress: repository.GetRandomHash(),
					ToAddress:   repository.GetRandomHash(),
					Amount:      "10",
				},
				{
					Timestamp:   "2021-11-28T02:43:48.881Z",
					FromAddress: repository.GetRandomHash(),
					ToAddress:   originalDepositAddr, // one of tx is for our deposit addr in DB!
					Amount:      "10",
				},
			}, nil).Once()

			client.On("SendJobcoin", mock.MatchedBy(func(depositAddr string) bool {
				s.Equal(originalDepositAddr, depositAddr)
				return true
			}), mock.MatchedBy(func(destinationAddr string) bool {
				s.Contains(destinationAddrs, destinationAddr)
				return true
			}), mock.MatchedBy(func(amt string) bool {
				// this can be 0 so below check isn't that strict
				s.NotEqual(amt, "0.0")
				return true
			})).Return(fmt.Errorf("unable to process request")).Times(len(destinationAddrs))
			// should be called exactly no of destination addresses times.
		}
		populateDB := func() {
			repository.GetDB().Store(
				originalDepositAddr,
				destinationAddrs,
			)
		}
		prepareMockResponses()
		populateDB()

		algo := algorithm.RandomDistribution{}
		m := ByTransaction{
			Client:    client,
			Algorithm: &algo,
		}

		// we can have text fixtures,
		// but as its just simple map so doing below way.
		quit := make(chan struct{})
		go m.Poll(pollingInterval, quit)
		// quit goroutine
		time.Sleep(1 * time.Second)
		quit <- struct{}{}
	})
}

func TestPollByTransactionsTestSuite(t *testing.T) {
	suite.Run(t, new(PollByTransactionsTestSuite))
}
