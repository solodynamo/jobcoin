package algorithm

import (
	"fmt"
	"testing"

	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/suite"
	"gitlab.com/solodynamo/jobcoin/pkg/utils"
)

type RandomDistributionTestSuite struct {
	suite.Suite
}

func (s *RandomDistributionTestSuite) TestAmountDistribution() {
	s.Run("int totalBalance = 5", func() {
		accounts := []string{
			uuid.NewV4().String(),
			uuid.NewV4().String(),
			uuid.NewV4().String(),
			uuid.NewV4().String(),
		}
		totalBalance := "5"

		m := RandomDistribution{}

		distribution, err := m.GetBalances(accounts, totalBalance)
		s.Require().NoError(err)
		s.Require().Equal(4, len(distribution))
		var sumWeGot float64 = 0.0
		for i := 0; i < len(distribution); i++ {
			sumWeGot += distribution[i]
		}
		floatTotalBalance, err := utils.GetFloat64(totalBalance)
		s.Require().NoError(err)
		s.Require().Equal(floatTotalBalance, sumWeGot, "amount sum after and before distribution should be same")
	})

	s.Run("float totalBalance = 100000.038", func() {
		accounts := []string{
			uuid.NewV4().String(),
			uuid.NewV4().String(),
			uuid.NewV4().String(),
			uuid.NewV4().String(),
		}
		totalBalance := "100000.038329"

		m := RandomDistribution{}

		distribution, err := m.GetBalances(accounts, totalBalance)
		s.Require().NoError(err)
		s.Require().Equal(4, len(distribution))
		var sumWeGot float64 = 0.0
		for i := 0; i < len(distribution); i++ {
			sumWeGot += distribution[i]
		}
		floatTotalBalance, err := utils.GetFloat64(totalBalance)
		s.Require().NoError(err)
		s.Require().Equal(fmt.Sprintf("%.6f", floatTotalBalance), fmt.Sprintf("%.6f", sumWeGot), "amount sum after and before distribution should be same")
	})

	s.Run("non parsable float totalBalance = mybal", func() {
		accounts := []string{
			uuid.NewV4().String(),
			uuid.NewV4().String(),
			uuid.NewV4().String(),
			uuid.NewV4().String(),
		}
		totalBalance := "mybal"

		m := RandomDistribution{}

		distribution, err := m.GetBalances(accounts, totalBalance)
		s.Require().Error(err)
		s.Require().Equal(0, len(distribution))
	})

}

func TestRandomDistributionTestSuite(t *testing.T) {
	suite.Run(t, new(RandomDistributionTestSuite))
}
