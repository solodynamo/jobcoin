package algorithm

import (
	"testing"

	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/suite"
	"gitlab.com/solodynamo/jobcoin/pkg/utils"
)

type UniformDistributionTestSuite struct {
	suite.Suite
}

func (s *UniformDistributionTestSuite) TestAmountDistribution() {
	s.Run("int totalBalance = 5", func() {
		accounts := []string{
			uuid.NewV4().String(),
			uuid.NewV4().String(),
			uuid.NewV4().String(),
			uuid.NewV4().String(),
		}
		totalBalance := "5"

		m := UniformDistribution{}

		distribution, err := m.GetBalances(accounts, totalBalance)
		s.Require().NoError(err)
		s.Require().Equal(4, len(distribution))
		var sumWeGot float64 = 0.0
		for i := 0; i < len(distribution); i++ {
			sumWeGot += distribution[i]
		}
		floatTotalBalance, err := utils.GetFloat64(totalBalance)
		s.Require().NoError(err)
		s.Require().Equal(floatTotalBalance, sumWeGot, "amount sum after and before distribution should be same")
	})

	s.Run("float totalBalance = 100000.038329", func() {
		accounts := []string{
			uuid.NewV4().String(),
			uuid.NewV4().String(),
			uuid.NewV4().String(),
			uuid.NewV4().String(),
		}
		totalBalance := "100000.038329"

		m := UniformDistribution{}

		distribution, err := m.GetBalances(accounts, totalBalance)
		s.Require().NoError(err)
		s.Require().Equal(4, len(distribution))
		var sumWeGot float64 = 0.0
		for i := 0; i < len(distribution); i++ {
			sumWeGot += distribution[i]
		}
		floatTotalBalance, err := utils.GetFloat64(totalBalance)
		s.Require().NoError(err)
		s.Require().Equal(floatTotalBalance, sumWeGot, "amount sum after and before distribution should be same")
	})

	s.Run("non parsable float totalBalance = mybal", func() {
		accounts := []string{
			uuid.NewV4().String(),
			uuid.NewV4().String(),
			uuid.NewV4().String(),
			uuid.NewV4().String(),
		}
		totalBalance := "mybal"

		m := UniformDistribution{}

		distribution, err := m.GetBalances(accounts, totalBalance)
		s.Require().Error(err)
		s.Require().Equal(0, len(distribution))
	})
}

func TestUniformDistributionTestSuite(t *testing.T) {
	suite.Run(t, new(UniformDistributionTestSuite))
}
