package algorithm

// DistributionAlgorithm defines interface for any distribution algorithm implemented
type DistributionAlgorithm interface {
	GetBalances(accounts []string, balance string) ([]float64, error)
}
