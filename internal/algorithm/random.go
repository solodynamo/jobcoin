package algorithm

import (
	"math/rand"
	"time"

	"gitlab.com/solodynamo/jobcoin/internal/infrastructure/logger"
	"gitlab.com/solodynamo/jobcoin/pkg/utils"
)

func init() {
	// rand.float64 uses global rand
	// ff Seed is not called, the rand generator behaves as if seeded by Seed(1)
	rand.Seed(time.Now().UnixNano())
}

// RandomDistribution implements DistributionAlgorithm interface to distribute amount using random distribution
type RandomDistribution struct {
}

func (m *RandomDistribution) GetBalances(accounts []string, balance string) ([]float64, error) {
	bl, err := utils.GetFloat64(balance)
	if err != nil {
		logger.Println("unable to convert total amount to float64")
		return []float64{}, err
	}
	numOfAddresses := len(accounts)
	var amountsToEachAddress []float64
	for idx := range accounts {
		if idx == numOfAddresses-1 {
			amountsToEachAddress = append(amountsToEachAddress, bl)
		} else {
			min := 0.25
			max := 0.50
			// rand.Float64 return pseudo-random float number between [0..1)
			// projecting that to the range of [min..max)
			amountsToAddress := min + rand.Float64()*(max-min)
			amountToAddress := bl * amountsToAddress
			bl = bl - amountToAddress
			amountsToEachAddress = append(amountsToEachAddress, amountToAddress)
		}
	}
	return amountsToEachAddress, nil
}
