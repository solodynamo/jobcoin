package algorithm

import (
	"gitlab.com/solodynamo/jobcoin/internal/infrastructure/logger"
	"gitlab.com/solodynamo/jobcoin/pkg/utils"
)

// UniformDistribution implements DistributionAlgorithm interface to distribute amount using uniform distribution
type UniformDistribution struct {
}

func (m *UniformDistribution) GetBalances(accounts []string, balance string) ([]float64, error) {
	bl, err := utils.GetFloat64(balance)
	if err != nil {
		logger.Println("unable to convert total amount to float64")
		return []float64{}, err
	}
	numOfAddresses := len(accounts)
	amountGoingToAddress := bl / float64(numOfAddresses)
	var amountsToEachAddress []float64
	for range accounts {
		amountsToEachAddress = append(amountsToEachAddress, amountGoingToAddress)
	}
	return amountsToEachAddress, nil
}
