package crud

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/suite"
)

type CRUDTestSuite struct {
	suite.Suite
}

func (s *CRUDTestSuite) TestInitiateMixing() {

	s.Run("201 case", func() {
		fn := InitiateMixingHandler()
		recorder := httptest.NewRecorder()
		handler := http.HandlerFunc(fn)

		reqBody := []byte(`
		{
			"destination_addresses": [
				"addr-one",
				"addr-two",
				"addr-three"
			]
		}
	`)
		r, _ := http.NewRequest("POST", "/mix", bytes.NewReader(reqBody))
		handler.ServeHTTP(recorder, r)
		s.Require().Equal(http.StatusCreated, recorder.Code)
	})

	s.Run("400 case", func() {
		fn := InitiateMixingHandler()
		recorder := httptest.NewRecorder()
		handler := http.HandlerFunc(fn)

		reqBody := []byte(`
		{
			"destination_addresses": "addr-one",
		}
	`)
		r, _ := http.NewRequest("POST", "/mix", bytes.NewReader(reqBody))
		handler.ServeHTTP(recorder, r)
		s.Require().Equal(http.StatusBadRequest, recorder.Code)
	})
}

func TestCRUDTestSuite(t *testing.T) {
	suite.Run(t, new(CRUDTestSuite))
}
