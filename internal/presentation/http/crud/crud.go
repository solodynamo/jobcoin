package crud

import (
	"encoding/json"
	"net/http"

	"gitlab.com/solodynamo/jobcoin/domain"
	"gitlab.com/solodynamo/jobcoin/internal/infrastructure/logger"
	"gitlab.com/solodynamo/jobcoin/internal/presentation/http/validation"
	"gitlab.com/solodynamo/jobcoin/internal/repository"
)

// InitiateMixingHandler is the handler to accept mixing request for addresses
// passed from the client
// it requires passed addresses to be an array, otherwise it return 400
func InitiateMixingHandler() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		logger.Println("got request for mixing..")
		var (
			payload domain.InitiateMixingRequest
			res     domain.InitiateMixingResponse
		)
		_ = json.NewDecoder(r.Body).Decode(&payload)
		// we can have stricter validations here which checks type,
		// address format, etc.
		if validation.InValid(payload) {
			respondWithJSON(w, http.StatusBadRequest, res)
			return
		}
		// we internally will store destination address(s) based on our
		// random generated deposit address
		depositAddress := repository.GetRandomHash()
		res.DepositAddress = depositAddress
		repository.Set(depositAddress, payload.DestinationAddresses)
		respondWithJSON(w, http.StatusCreated, res)
	}
}

func respondWithJSON(w http.ResponseWriter, status int, payload interface{}) {
	response, _ := json.Marshal(payload)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	w.Write(response)
}
