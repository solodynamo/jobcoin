package validation

import (
	"testing"

	"github.com/stretchr/testify/suite"
	"gitlab.com/solodynamo/jobcoin/domain"
)

type ValidationTestSuite struct {
	suite.Suite
}

func (s *ValidationTestSuite) TestInValid() {
	s.Run("valid case", func() {
		m := domain.InitiateMixingRequest{
			DestinationAddresses: []string{"addr-1", "addr-2"},
		}
		s.Require().False(InValid(m))
	})
	s.Run("invalid case", func() {
		m := domain.InitiateMixingRequest{
			DestinationAddresses: []string{},
		}
		s.Require().True(InValid(m))
	})
}

func TestValidationTestSuite(t *testing.T) {
	suite.Run(t, new(ValidationTestSuite))
}
