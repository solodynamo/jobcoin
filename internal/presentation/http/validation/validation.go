package validation

import (
	"reflect"

	"gitlab.com/solodynamo/jobcoin/domain"
)

// InValid performs payload validation for InitiateMixingRequest handler
func InValid(m domain.InitiateMixingRequest) bool {
	// we can add validation logic before even allowing the payload to reach core of our logic
	t := reflect.TypeOf(m.DestinationAddresses).Kind()
	if t != reflect.Slice && t != reflect.Array {
		return true
	}
	if (len(m.DestinationAddresses)) == 0 {
		return true
	}
	return false
}
