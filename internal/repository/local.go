package repository

import (
	"sync"

	uuid "github.com/satori/go.uuid"
)

var depositAddressDB sync.Map

// GetDB is our simple local concurrent hash map
// it is safe for concurrent use
// by multiple goroutines without additional locking or coordination.
// Data isn't persisted and it will stay till the server isn't restarted
func GetDB() *sync.Map {
	return &depositAddressDB
}

// Set sets value in map safely
func Set(depositAddress string, DestinationAddresses []string) {
	GetDB().Store(depositAddress, DestinationAddresses)
}

// Get gets value from map safely
func Get(depositAddress string) ([]string, bool) {
	vals, isPresent := GetDB().Load(depositAddress)
	if !isPresent {
		return []string{}, isPresent
	}
	return vals.([]string), true
}

// GetRandomHash return hash for address(deposit, etc)
// it uses UUID v4 version
func GetRandomHash() string {
	return uuid.NewV4().String()
}
