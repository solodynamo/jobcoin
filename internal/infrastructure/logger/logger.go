package logger

import "log"

// Println prints log to stdout.
// ideally there will be json formatted logger which stdouts the json log which will be forwarded to some system like ELK stack,datadog,newrelic, etc
func Println(str string) {
	log.Println(str)
}
