package jobcoin

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"

	"gitlab.com/solodynamo/jobcoin/config"
	"gitlab.com/solodynamo/jobcoin/domain"
	"gitlab.com/solodynamo/jobcoin/internal/infrastructure/logger"
)

var (
	addressAPI      = config.Get().GeminiBaseURL + "/addresses"
	transactionsAPI = config.Get().GeminiBaseURL + "/transactions"
)

// HTTPClient represents ops for an http client
type HTTPClient interface {
	Do(req *http.Request) (*http.Response, error)
}

// APIClient represents interface to interact with jobcoin API
type APIClient interface {
	GetAddressInfo(address string) (domain.JobcoinAddress, error)
	SendJobcoin(fromAddress, toAddress, amount string) error
	GetTransactions() ([]domain.JobcoinTx, error)
}

// JobcoinClient is an implementation of APIClient interface
type JobcoinClient struct {
	Client HTTPClient
}

// GetAddressInfo returns address info for given address
func (jc *JobcoinClient) GetAddressInfo(address string) (domain.JobcoinAddress, error) {
	url := fmt.Sprintf("%s/%s", addressAPI, address)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Println(err)
		return domain.JobcoinAddress{}, err
	}

	res, err := jc.Client.Do(req)
	if err != nil {
		log.Println(err)
		return domain.JobcoinAddress{}, err
	}
	defer res.Body.Close()

	var addrInfo domain.JobcoinAddress
	err = json.NewDecoder(res.Body).Decode(&addrInfo)
	if err != nil {
		log.Println(err)
		return domain.JobcoinAddress{}, err
	}

	return addrInfo, nil
}

// GetTransactions returns all transactions info
func (jc *JobcoinClient) GetTransactions() ([]domain.JobcoinTx, error) {
	req, err := http.NewRequest("GET", transactionsAPI, nil)
	if err != nil {
		logger.Println(fmt.Sprintf("error in creating get transactions API call %v", err))
		return []domain.JobcoinTx{}, err
	}

	res, err := jc.Client.Do(req)
	if err != nil {
		logger.Println(fmt.Sprintf("error in initiating get transactions API call %v", err))
		return []domain.JobcoinTx{}, err
	}
	defer res.Body.Close()

	var txnInfo []domain.JobcoinTx
	err = json.NewDecoder(res.Body).Decode(&txnInfo)
	if err != nil {
		logger.Println(fmt.Sprintf("error in decoding get transaction info %v", err))
		return []domain.JobcoinTx{}, err
	}

	return txnInfo, nil
}

// SendJobcoin sends an amount to a given address
func (jc *JobcoinClient) SendJobcoin(fromAddress, toAddress, amount string) error {
	reqBody, err := json.Marshal(domain.JobcoinTx{
		FromAddress: fromAddress,
		ToAddress:   toAddress,
		Amount:      amount,
	})
	if err != nil {
		logger.Println(fmt.Sprintf("error in marshal post body for transactions API call %v", err))
		return err
	}

	req, err := http.NewRequest("POST", transactionsAPI, bytes.NewReader(reqBody))
	if err != nil {
		logger.Println(fmt.Sprintf("error in creating post transactions API call %v", err))
		return err
	}
	req.Header.Set("Content-Type", "application/json")

	res, err := jc.Client.Do(req)
	if err != nil {
		logger.Println(fmt.Sprintf("error in initiating post transactions API call %v", err))
		return err
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		apiErr := struct {
			Error interface{} `json:"error"`
		}{}
		err = json.NewDecoder(res.Body).Decode(&apiErr)
		if err != nil {
			logger.Println(fmt.Sprintf("error in decoding error response from post transactions API call %v", apiErr.Error))
			return err
		}
		errMessage := fmt.Sprintf("Failed to create transaction due to: %s", apiErr.Error)
		logger.Println(fmt.Sprintf("non ok status code from post transactions API call %v", apiErr.Error))
		return errors.New(errMessage)
	}

	return nil
}
