package utils

import "strconv"

func GetFloat64(amount string) (float64, error) {
	val, err := strconv.ParseFloat(amount, 64)
	if err != nil {
		return 0.0, err
	}
	return val, nil
}
