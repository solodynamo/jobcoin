package utils

import (
	"testing"

	"github.com/stretchr/testify/suite"
)

type UtilsTestSuite struct {
	suite.Suite
}

func (s *UtilsTestSuite) TestGetFloat64() {
	s.Run("non decimal number", func() {
		n, err := GetFloat64("125")
		s.Require().NoError(err)
		s.Require().Equal(float64(125), n)
	})

	s.Run("decimal number", func() {
		n, err := GetFloat64("125.24")
		s.Require().NoError(err)
		s.Require().Equal(float64(125.24), n)
	})

	s.Run("extreme decimal number", func() {
		n, err := GetFloat64("0.243849284939393939393839")
		s.Require().NoError(err)
		s.Require().Equal(float64(0.243849284939393939393839), n)
	})

	s.Run("invalid number", func() {
		n, err := GetFloat64("invalid")
		s.Require().Error(err)
		s.Require().Equal(float64(0.0), n)
	})
}

func TestUtilsTestSuite(t *testing.T) {
	suite.Run(t, new(UtilsTestSuite))
}
